import { shallowMount } from "@vue/test-utils";
import VwCheckbox from "@/components/atoms/VwCheckbox";

describe("VwCheckbox", () => {
  const firstClick = { check: true, element: { id: 1, name: "test" } };

  it("create s snapshot", () => {
    const wrapper = shallowMount(VwCheckbox);
    expect(wrapper.vm.$el).toMatchSnapshot();
  });

  it("create s snapshot with active", () => {
    const isChecked = true;
    const wrapper = shallowMount(VwCheckbox, {
      propsData: { isChecked }
    });
    expect(wrapper.vm.$el).toMatchSnapshot();
  });

  it("create s snapshot with isPartialChecked", () => {
    const isPartialChecked = true;
    const wrapper = shallowMount(VwCheckbox, {
      propsData: { isPartialChecked }
    });
    expect(wrapper.vm.$el).toMatchSnapshot();
  });

  it("first click element equal 'true' and props", () => {
    const element = { id: 1, name: "test" };
    const wrapper = shallowMount(VwCheckbox, {
      propsData: { element }
    });
    wrapper.find("div.vw-checkbox").trigger("click");
    const result = wrapper.emitted().statusCheck[0][0];
    expect(result).toEqual(firstClick);
  });

  it("active must be false", () => {
    const wrapper = shallowMount(VwCheckbox, {});
    const active = wrapper.vm.active;
    expect(active).toBeFalsy();
  });

  it("classCheck must be empty", () => {
    const wrapper = shallowMount(VwCheckbox, {});
    const classCheck = wrapper.vm.classChecked;
    expect(classCheck).toBe("");
  });

  it("classCheck must be 'vw-checkbox--checked'", () => {
    const isChecked = true;
    const wrapper = shallowMount(VwCheckbox, {
      propsData: { isChecked }
    });
    const classCheck = wrapper.vm.classChecked;
    expect(classCheck).toBe("vw-checkbox--checked");
  });

  it("showCheckIcon must be false", () => {
    const wrapper = shallowMount(VwCheckbox, {});
    const checkIcon = wrapper.vm.showCheckIcon;
    expect(checkIcon).toBeFalsy();
  });

  it("showCheckIcon must be true", () => {
    const isChecked = true;
    const wrapper = shallowMount(VwCheckbox, {
      propsData: { isChecked }
    });
    const checkIcon = wrapper.vm.showCheckIcon;
    expect(checkIcon).toBeTruthy();
  });

  it("active must be true when passed prop isChecked", () => {
    const isChecked = true;
    const wrapper = shallowMount(VwCheckbox, {
      propsData: { isChecked }
    });
    const active = wrapper.vm.active;
    expect(active).toBeTruthy();
  });
});
