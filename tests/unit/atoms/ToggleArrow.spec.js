import { shallowMount } from "@vue/test-utils";
import ToggleArrow from "@/components/atoms/ToggleArrow";

describe("ToggleArrow", () => {
  const firstClick = { active: true, element: { id: 1, name: "test" } };
  it("create a snapshot", () => {
    const wrapper = shallowMount(ToggleArrow);
    expect(wrapper.vm.$el).toMatchSnapshot();
  });

  it("create a snapshot when opened", () => {
    const isActive = true;
    const wrapper = shallowMount(ToggleArrow, {
      propsData: { isActive }
    });
    expect(wrapper.vm.$el).toMatchSnapshot();
  });

  it("first click element equal 'true' and props", () => {
    const element = { id: 1, name: "test" };
    const wrapper = shallowMount(ToggleArrow, {
      propsData: { element }
    });
    wrapper.find("div.toggle-arrow").trigger("click");
    const result = wrapper.emitted().statusActive[0][0];
    expect(result).toEqual(firstClick);
  });

  it("toggle active must be false", () => {
    const wrapper = shallowMount(ToggleArrow, {});
    const active = wrapper.vm.active;
    expect(active).toBeFalsy();
  });

  it("toggle active must be true", () => {
    const isActive = true;
    const wrapper = shallowMount(ToggleArrow, {
      propsData: { isActive }
    });
    const active = wrapper.vm.active;
    expect(active).toBeTruthy();
  });

  it("activeArrow must be empty", () => {
    const wrapper = shallowMount(ToggleArrow, {});
    const activeArrow = wrapper.vm.activeArrow;
    expect(activeArrow).toBe("");
  });

  it("activeArrow must be 'toggle-arrow__icon--up'", () => {
    const isActive = true;
    const wrapper = shallowMount(ToggleArrow, {
      propsData: { isActive }
    });
    const activeArrow = wrapper.vm.activeArrow;
    expect(activeArrow).toBe("toggle-arrow__icon--up");
  });
});
